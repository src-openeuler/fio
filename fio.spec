Name:		fio
Version:	3.39
Release:	1
Summary:	Multithreaded IO generation tool

License:	GPL-2.0-only
URL:		http://git.kernel.dk/?p=fio.git;a=summary
Source0:        http://brick.kernel.dk/snaps/%{name}-%{version}.tar.bz2

BuildRequires:  gcc
BuildRequires:	libaio-devel
BuildRequires:	zlib-devel
BuildRequires:	python3-devel
BuildRequires:	libcurl-devel
BuildRequires:	openssl-devel
%ifnarch s390
BuildRequires:  gperftools-devel
%endif
%ifarch x86_64 ppc64le
BuildRequires:	libpmem-devel
BuildRequires:	libpmemblk-devel
%endif

BuildRequires: make
Requires: python3-pandas

%global __provides_exclude_from ^%{_libdir}/fio/


%description
fio is an I/O tool that will spawn a number of threads or processes doing
a particular type of io action as specified by the user.  fio takes a
number of global parameters, each inherited by the thread unless
otherwise parameters given to them overriding that setting is given.
The typical use of fio is to write a job file matching the io load
one wants to simulate.
	

%package_help

%prep
%autosetup -p1

pathfix.py -i %{__python3} -pn \
 tools/fio_jsonplus_clat2csv \
 tools/fiologparser.py \
 tools/hist/*.py \
 tools/plot/fio2gnuplot \
 t/steadystate_tests.py

# Edit /usr/local/lib path in os/os-linux.h to match Fedora conventions.
sed -e 's,/usr/local/lib/,%{_libdir}/,g' -i os/os-linux.h

%build
./configure --disable-optimizations --dynamic-libengines

EXTFLAGS="$RPM_OPT_FLAGS" LDFLAGS="$RPM_LD_FLAGS" %make_build

%install
make install prefix=%{_prefix} mandir=%{_mandir} libdir=%{_libdir}/fio DESTDIR=$RPM_BUILD_ROOT INSTALL="install -p"

%check
make test

%files
%doc COPYING
%{_bindir}/*
%{_datadir}/%{name}
%{_libdir}/fio/*.so

%files help
%doc REPORTING-BUGS examples MORAL-LICENSE GFIO-TODO SERVER-TODO STEADYSTATE-TODO
%{_mandir}/man1/*


%changelog
* Mon Feb 17 2025 lvfei <lvfei@kylinos.cn> - 3.39-1
- upgrade to 3.39
- t/run-fio-tests: remove redundant pre_success lines
- t/jobs/t0036,0037: add tests for verify_state_save/load option

* Sun Nov 03 2024 Funda Wang <fundawang@yeah.net> - 3.38-1
- update to 3.38

* Fri Jul 05 2024 wangkai <13474090681@163.com> - 3.37-1
- Update to 3.37
- Change: https://git.kernel.dk/?p=fio.git;a=shortlog;h=refs/tags/fio-3.37

* Tue Jun 4 2024 zhangyaqi <zhangyaqi@kylinos.cn> - 3.36-3
- engines/http: Fix memory leak 

* Mon Apr 15 2024 cenhuilin <cenhuilin@kylinos.cn> - 3.36-2
- iolog: fix disk stats issue

* Thu Mar 14 2024 xu_ping <707078654@qq.com> - 3.36-1
- Update to 3.36

* Tue Mar 5 2024 wangxiaomeng <wangxiaomeng@klinos.cn> - 3.34-2
- Add BuildRequires gperftools-devel to link tcmalloc.

* Thu Apr 27 2023 liyanan <thistleslyn@163.com> - 3.34-1
- Update to 3.34

* Sun Nov 6 2022 huyab<1229981468@qq.com> - 3.32-1
- update version to 3.32-1

* Sun Aug 14 2022 tianlijing <tianlijing@kylinos.cn> - 3.30-1
- upgrade to 3.30

* Fri Jan 14 2022 caodongxia <caodongxia@huawei.com> - 3.29-1
- Upgrade 3.29

* Fri Jul 30 2021 linjiaxin5 <linjiaxin5@huawei.com> - 3.7-11
- Fix failure caused by GCC upgrade to 10

* Wed Jul 21 2021 lingsheng <lingsheng@huawei.com> - 3.7-10
- Remove unnecessary buildrequire gdb

* Wed Jun 2 2021 baizhonggui <baizhonggui@huawei.com> - 3.7-9
- Fix building error: configure: failed to find compiler
- Add gcc in BuildRequires

* Tue Nov 03 2020 lingsheng <lingsheng@huawei.com> - 3.7-8
- fio2gnuplot: fix TabErrors when running with Python 3

* Sat Sep 19 2020 yanan li <liyanan032@huawei.com> - 3.7-7
- Modify python2.7 to python3 with requires

* Thu Jul 23 2020 wutao<wutao61@huawei.com> - 3.7-5
- fix build error because of updating glibc

* Sat Mar 21 2020 huzunhao<huzunhao@huawei.com> - 3.7-4
- Type: NA
- ID: NA
- SUG: NA
- DESC: add buildrequire gdb

* Wed Nov 27 2019 likexin<likexin4@huawei.com> - 3.7-3
- Package init
